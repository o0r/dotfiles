" Plugin list
call plug#begin(stdpath('data') . '/plugged')
  Plug 'dracula/vim', { 'as': 'dracula' }
  Plug 'vim-airline/vim-airline'

  Plug 'airblade/vim-gitgutter'
  Plug 'cloudhead/neovim-fuzzy'
  
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  
  Plug 'hashivim/vim-terraform'

  Plug 'pangloss/vim-javascript'
  Plug 'mxw/vim-jsx'

  Plug 'ryanoasis/vim-devicons'

  Plug 'editorconfig/editorconfig-vim'
  Plug 'patstockwell/vim-monokai-tasty'

  Plug 'vim-scripts/PreserveNoEOL'
call plug#end()

" Plugin options
source ~/.config/nvim/dracula.vim
source ~/.config/nvim/airline.vim
source ~/.config/nvim/gitgutter.vim
source ~/.config/nvim/coc.vim
source ~/.config/nvim/terraform.vim

" Vim settings
source ~/.config/nvim/shortcuts.vim

" Use clipboard
set clipboard=unnamedplus

