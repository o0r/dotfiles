" Use tab for trigger completion with characters ahead and navigate.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Extensions
let g:coc_global_extensions = ['coc-explorer', 'coc-pairs', 'coc-json', 'coc-tsserver', 'coc-css', 'coc-python', 'coc-eslint', 'coc-stylelint', 'coc-go', 'coc-docker']

:nmap <Leader>e :CocCommand explorer<CR>
:nmap <Leader>d :CocDiagnostics<CR>
