" show always diff column
set signcolumn=yes

" reduce updating time of diff column
set updatetime=100
