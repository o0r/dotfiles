" Set jk to esc
:imap jk <Esc>

" Move line up or down (shift + up, shift + down)
nnoremap <S-Up> :m-2<CR>
nnoremap <S-Down> :m+<CR>
inoremap <S-Up> <Esc>:m-2<CR>
inoremap <S-Down> <Esc>:m+<CR>

" Write file with leader + w
nnoremap <Leader>w :w<cr>

" Exit with leader + q
nnoremap <Leader>q :q<cr>

" Exit all
nnoremap <Leader>qa :qall<cr>

" Exit without saving
nnoremap <Leader>qq :q!<cr>

" Exit all without saving
nnoremap <Leader>qaq :qall!<cr>

" Write and exit with leader +Q
nnoremap <Leader>wq :wq<cr>

" Write all
nnoremap <Leader>wa :wall<cr>

" Write all and quit
nnoremap <Leader>wqa :wqall<cr>

" Open file with leader + o
nnoremap <Leader>o :FuzzyOpen<cr>

" Next buffer leader + ]
nnoremap <Leader>] :bn<cr>

" Prev buffer leader + [
nnoremap <Leader>[ :bp<cr>

" Close buffer
nnoremap <Leader><BS> :bd<cr>
nnoremap <Leader><BS><BS> :bd!<cr>

