" Set airline theme
let g:airline_theme='dracula'
" let g:airline_theme='monokai_tasty'

" Set airline fonts
let g:airline_powerline_fonts = 1

" Set tabline
let g:airline#extensions#tabline#enabled = 1

" Show buffer number
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1

