" Set dracula theme
colorscheme dracula

" Set true colors
set termguicolors

" Enable syntax
syntax enable

" Highlight current line
set cursorline

" Show line number
set number

